from setuptools import setup

setup(name='dfx_client_api',
      version='0.2',
      description='The dfx_client_api package is available for analysts who want to integrate DiamondFX application data within their own data transformation pipeline from a python interface.',
      url='http://github.com/storborg/funniest',
      author='TrinityVR',
      author_email='support@trinityvr.com',
      license='MIT',
      packages=['dfx_client_api'],
    install_requires=[
                      'datetime','requests'
                  ],
      zip_safe=False)