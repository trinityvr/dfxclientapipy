import time
import datetime
import requests

# base DFX Client API url
base_url = "https://api.trinity-dfx.com/clientdata"

def process_request(url, api_key):

    header = {'authorization': "bearer: " + api_key}
    data = requests.get(url, headers=header)
    data = data.json()

    #print(data)
    return(data)


def get_pitch_recognition_data(api_key, start_date=str(datetime.datetime.fromtimestamp(time.time() - 604800).strftime('%Y-%m-%d %H:%M:%S')), end_date=str(datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S'))):
    pr_url = "/pitchrecognition"
    url = base_url + pr_url + "?" + "startDate=" + start_date + "&" + "endDate=" + end_date
    print(url)

    data = process_request(url, api_key)
    return(data)


def get_batting_sim_data(api_key, start_date=str(datetime.datetime.fromtimestamp(time.time() - 604800).strftime('%Y-%m-%d %H:%M:%S')), end_date=str(datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S'))):
    bs_url = "/battingsimulation"
    url = base_url + bs_url + "?" + "startDate=" + start_date + "&" + "endDate=" + end_date
    print(url)

    data = process_request(url, api_key)
    return(data)








